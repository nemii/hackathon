import './App.css';
import SettingsIcon from '@material-ui/icons/Settings';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

const useStyles = makeStyles((theme) => ({
  rounded: {
    color: '#fff',
    backgroundColor: '#3A3A3C',
    height: 25,
    width: 25
  },
  rounded2: {
    color: '#fff',
    backgroundColor: '#3A3A3C',
    height: 36,
    width: 36
  },
}));

function App() {
  const classes = useStyles();

  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item text-start">
                <a className="nav-link f-16" aria-current="page" href="#">Telegram</a>
              </li>
              <li className="nav-item text-start">
                <a className="nav-link f-16" href="#">Discord</a>
              </li>
              <li className="nav-item text-start">
                <a className="nav-link f-16" href="#">Github</a>
              </li>
            </ul>
            <form className="d-flex">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a className="nav-link text-start f-16" aria-current="page" href="#">Litepaper</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-start f-16" href="#">Whitepaper</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-start" href="#">
                    <Avatar variant="rounded" className={classes.rounded}>
                      <SettingsIcon fontSize="small" />
                    </Avatar>
                  </a>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </nav>





      <nav class="navbar navbar-expand-lg navbar-dark bg-dark border-top-light">
        <div class="container">
          <a class="navbar-brand" href="#">
            <img src="/logo.svg" className="img-fluid" style={{ width: '80%' }} />
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form class="d-flex" style={{ flex: 1 }}></form>
            <form class="d-flex" style={{ flex: 2 }}>
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Enterprise
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="#">Enterprise 1</a></li>
                    <li><a class="dropdown-item" href="#">Enterprise 2</a></li>
                    <li><a class="dropdown-item" href="#">Enterprise 3</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="Individuals" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Individuals
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="Individuals">
                    <li><a class="dropdown-item" href="#">Individuals 1</a></li>
                    <li><a class="dropdown-item" href="#">Individuals 2</a></li>
                    <li><a class="dropdown-item" href="#">Individuals 3</a></li>
                  </ul>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="Developers" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Developers
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="Developers">
                    <li><a class="dropdown-item" href="#">Developers 1</a></li>
                    <li><a class="dropdown-item" href="#">Developers 2</a></li>
                    <li><a class="dropdown-item" href="#">Developers 3</a></li>
                  </ul>
                </li>
              </ul>
            </form>
            <form class="d-flex">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a className="nav-link text-start" href="#">
                    <Avatar variant="rounded" className={classes.rounded2}>
                      <MoreHorizIcon />
                    </Avatar>
                  </a>
                </li>
                <li className="nav-item mt-auto mb-auto" style={{ width: '130px' }}>
                  <button type="button" class="btn btn-primary btn-sm w-100 p-2" style={{ background: '#F85E11', border: 'none', outline: 'none' }}>Launch App</button>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </nav>
      <div style={{ backgroundColor: '#3A3A3C', backgroundImage: 'url(./landing4.svg)', minHeight: '84vh', backgroundSize: 'contain' }}>
        <div style={{ backgroundImage: 'url(./landing.png)', minHeight: '84vh', backgroundSize: 'cover' }}>
          <div style={{ backgroundImage: 'url(./landing2.svg)', minHeight: '90vh', backgroundSize: 'contain' }}>

            <div className="text-center">
              <img src="./worldwide.png" className="img-fluid mt-3 p-3" />
            </div>
            <div className="col-8 m-auto">
              <img src="./hack.png" className="mt-3 img-fluid" />
            </div>
            <div className="text-center pt-5">
              <img src="./sugarland.png" style={{ width: '35%' }} />
            </div>
            <div className="col-12 col-md-8 m-auto">
              <img src="./spring.svg" className="mt-5 img-fluid" />
            </div>
            <div className="col-12 col-md-12 text-center">
              <button type="button" class="btn btn-primary btn-sm p-2" style={{ background: 'transparent', outline: 'none', width: 160, border: '1px solid white' }}>Apply Now</button>
            </div>

            <div className="col-11 col-md-8 m-auto">
              <div className="row mt-3">
                <div className="col-3 col-md-1 m-auto text-white mt-2">Winners</div>
                <div className="col-3 col-md-1 m-auto text-white mt-2">Schedule</div>
                <div className="col-3 col-md-1 m-auto text-white mt-2">Judges</div>
                <div className="col-3 col-md-1 m-auto text-white mt-2">Technology</div>
                <div className="col-3 col-md-1 m-auto text-white mt-2">Participants</div>
                <div className="col-3 col-md-1 m-auto text-white mt-2">Resources</div>
                <div className="col-3 col-md-1 m-auto text-white mt-2">FAQs</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container mt-5 mb-5">
        <div className="col-12 col-md-8 m-auto">
          <div className="row">
            <div className="col-12 col-md-6 mt-3">
              <img src="./video.png" className="img-fluid" />
            </div>
            <div className="col-12 col-md-6 mt-auto mb-auto">
              <div>
                <p className="mt-3" style={{
                  fontWeight: '400',
                  fontSize: 14
                }}>WINNERS</p>
                <p className="col-12 col-md-6" style={{
                  color: '#F85E11',
                  fontSize: 22
                }}>
                  $125k+ in Hackathon prize bounties
                </p>
                <p className="col-12 col-md-10" style={{
                  color: '#979797',
                  fontSize: 12
                }}>
                  Explore a diverse range of winning projects that built feature-rich smart contracts by accessing off-chain data and computation through Chainlink oracle networks.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container mb-5">
        <div className="col-12 col-md-8 m-auto">
          <div className="row">
            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#F85E11',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>


            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>

            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>


            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>


            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>



            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>



            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>


            <div className="col-12 col-md-3 mt-3">
              <div style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 10,
                backgroundColor: '#979797',
                borderRadius: 10,
                padding: 5
              }}>
                <div style={{
                  fontSize: 20,
                  marginBottom: 16,
                  marginTop: 10
                }}>
                  $8000
                </div>
                <p>
                  GRAND PRIZE
                </p>
                <p>
                COMPANY NAME HERE >
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div className="" style={{
        backgroundImage: 'url(./winner.png)',
        backgroundSize: 'cover',
        minHeight: '50vh'
      }}>
        <div className="container">
          <div className="col-12">
            <div className="row pt-5">
              <div className="col-12 col-md-5">
                <p style={{
                  color: 'white',
                  fontSize: 14
                }}>Runner-Up Awards</p>
                <p className="col-12 col-md-6" style={{
                  color: 'white',
                  fontSize: 22
                }}>
                  $125k+ in Hackathon prize bounties
                </p>
                <p style={{
                  color: 'white',
                  fontSize: 12
                }}>
                  Explore a diverse range of winning projects that built feature-rich smart contracts by accessing off-chain data and computation through Chainlink oracle networks.</p>
              </div>

              <div className="col-12 col-md-7 ">
                <p style={{
                  color: 'white',
                  fontSize: 12
                }}>
                COMPANY NAME HERE > COMPANY NAME HERE > COMPANY NAME HERE > COMPANY NAME HERE >
                </p>
                <p style={{
                  color: 'white',
                  fontSize: 12
                }}>
                COMPANY NAME HERE > COMPANY NAME HERE > COMPANY NAME HERE > COMPANY NAME HERE >
                </p>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-primary btn-sm p-2 mt-3" style={{ background: 'transparent', outline: 'none', border: '1px solid white' }}>SEE THE PROJECT GALLERY</button>
        </div>
      </div>


      <div style={{
        background: 'url(./schedule.png)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        minHeight: '80vh'
      }}>

        <p style={{
          color: 'white',
          fontSize: 14,
          textAlign: 'center',
          paddingTop: 50
        }}>SCHEDULE</p>

        <p style={{
          color: 'white',
          fontSize: 20,
          textAlign: 'center',
          paddingTop: 10
        }}>March 15 – April 11</p>

        <p className="col-12 col-md-4 m-auto" style={{
          color: 'white',
          fontSize: 12,
          textAlign: 'center',
          paddingTop: 10
        }}>
          Explore a diverse range of winning projects that built feature-rich smart contracts by accessing off-chain data and computation through Chainlink oracle networks.
        </p>

        <div className="container mb-5 pt-5">
          <div className="col-12 col-md-11 m-auto pb-5">
            <div className="row pb-5">
              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: 'white',
                  fontSize: 12,
                  backgroundColor: '#00000066',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#35DDE6'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>


              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: 'white',
                  fontSize: 12,
                  backgroundColor: '#00000066',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#35DDE6'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>

              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: 'white',
                  fontSize: 12,
                  backgroundColor: '#00000066',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#35DDE6'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>

              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: 'white',
                  fontSize: 12,
                  backgroundColor: '#00000066',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#35DDE6'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>


              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: 'white',
                  fontSize: 12,
                  backgroundColor: '#F85E11',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#35DDE6'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#35DDE6'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>


              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: '#3A3A3C',
                  fontSize: 12,
                  backgroundColor: '#EFEFEF',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#F85E11'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>


              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: '#3A3A3C',
                  fontSize: 12,
                  backgroundColor: '#EFEFEF',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#F85E11'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>



              <div className="col-12 col-md-3 mt-3">
                <div style={{
                  color: '#3A3A3C',
                  fontSize: 12,
                  backgroundColor: '#EFEFEF',
                  borderRadius: 10,
                  padding: 10
                }}>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 16,
                    marginTop: 10,
                    color: '#F85E11'
                  }}>
                    Date Here
                  </div>
                  <p>
                    Teambuilding Event & Gather
                  </p>
                  <p>
                    Intro to Solidity & Blockchain <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaling your Dapps using Polygon <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    IPFS workshop <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Scaffold-ETH with Austin <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                  <p>
                    Solana workshop <span style={{
                      color: '#3A3A3C'
                    }}>> Watch</span>
                  </p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="col-12">
          <div className="row">
            <div className="col-12 col-md-6">
              <p style={{
                fontSize: 14
              }}>JUDGES & SPEAKERS</p>

              <p className="col-12 col-md-6" style={{
                color: '#F85E11',
                fontSize: 22
              }}>
                Inspired by industry leaders
              </p>

              <p className="col-12 col-md-7" style={{
                fontSize: 12,
                color: '#979797'
              }}>Explore a diverse range of winning projects that built feature-rich smart contracts by accessing off-chain data and computation through Chainlink oracle networks.</p>
            </div>
          </div>
        </div>
      </div>

      <div className="container mb-5">
        <div className="col-12 col-md-12 m-auto">
          <div className="row">
            {[1, 2, 3, 4, 5, 6, 7, 8].map(v =>
              <div key={v} className="col-12 col-md-3 mt-5">
                <div style={{
                  textAlign: 'start',
                  color: 'white',
                  fontSize: 10,
                  backgroundColor: '#3A3A3C',
                  borderRadius: 10,
                  paddingLeft: 20,
                  paddingTop: 10,
                  paddingBottom: 5,
                  position: 'relative'
                }}>
                  <div style={{
                    position: 'absolute',
                    top: -30
                  }}>
                    <img src="./user.png" className="img-fluid" style={{ height: 90 }} />
                  </div>
                  <div style={{
                    fontSize: 20,
                    marginBottom: 10,
                    marginTop: 55
                  }}>
                    Name Here
                  </div>
                  <div>
                    Title
                  </div>
                  <p>
                COMPANY NAME HERE >
                  </p>
                </div>
              </div>)}
          </div>
        </div>
      </div>

      <div style={{
        background: '#242426',
        padding: 20
      }}>
        <div className="container mt-5">
          <div className="row">
            <div className="col-12 col-md-7">
              <div className="row">
                {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].map(v =>
                  <div key={v} className="col-6 col-md-4 mt-3 p-3" >
                    <img src="./solana.png" className="img-fluid" />
                  </div>)}
              </div>
            </div>
            <div className="col-12 col-md-5 mt-3 p-4">
              <p style={{
                color: 'white',
                fontSize: 12
              }}>TECHNOLOGY PARTICIPANTS</p>

              <p className="col-12 col-md-10" style={{
                color: 'white',
                fontSize: 22
              }}>
                Built with these market-leading protocols & organizations
              </p>

              <p className="col-12 col-md-8" style={{
                fontSize: 12,
                color: 'white'
              }}>Explore a diverse range of winning projects that built feature-rich smart contracts by accessing off-chain data and computation through Chainlink oracle networks.</p>
            </div>
          </div>
        </div>
      </div>


      <div style={{
        background: '#242426'
      }}>
        <div className="container">
          <div className="pt-5">
            <hr style={{ color: 'grey' }} />
          </div>
          <div className="text-white">
            How frequently will i receive rewards?
          </div>
          <div className="">
            <hr style={{ color: 'grey' }} />
          </div>


          <div className="text-white">
            Is there a minimum/maximum digital token holdings requirement to start staking?
          </div>
          <div className="">
            <hr style={{ color: 'grey' }} />
          </div>


          <div className="text-white">
            Is there a minimum/maximum digital token holdings requirement to start staking?
          </div>
          <div className="">
            <hr style={{ color: 'grey' }} />
          </div>

          <div className="text-white">
            Do i have to pay fees to use this service?
          </div>
          <div className="">
            <hr style={{ color: 'grey' }} />
          </div>


          <div className="text-white">
            Is it safe and how does USDAO stake my digital tokens?
          </div>
          <div className="">
            <hr style={{ color: 'grey' }} />
          </div>


          <div className="text-white">
            So i just leave my digital tokens in my USDAO account and they earn rewards?
          </div>
          <div className="pb-5">
            <hr style={{ color: 'grey' }} />
          </div>


        </div>

        <div className="container mt-5 pb-5">
          <div className="row">
            <div className="col-12 col-md-2 m-auto">
              <a class="navbar-brand" href="#">
                <img src="/logo.svg" className="img-fluid" style={{ width: '80%' }} />
              </a>
            </div>

            <div className="col-12 col-md-2 m-auto">
              <a class="navbar-brand text-white" href="#">
                Stake
              </a>
            </div>

            <div className="col-12 col-md-2 m-auto">
              <a class="navbar-brand text-white" href="#">
                Get USDAO
              </a>
            </div>

            <div className="col-12 col-md-2 m-auto">
              <a class="navbar-brand text-white" href="#">
                Community
              </a>
            </div>

            <div className="col-12 col-md-2 m-auto">
              <a class="navbar-brand text-white" href="#">
                Presskit
              </a>
            </div>

            <div className="col-12 col-md-2 m-auto">
              <a class="navbar-brand text-white" href="#">
                Privacy Policy
              </a>
            </div>
          </div>
        </div>

        <p style={{ fontSize: 14 }}>
          Digital assets are subject to a number of risks, including price volatility. Transacting digital assets could return in significant losses and may not be suitable for some consumers. Digital asset markets and exchanges are not regulated with the same controls or customer protections available with other forms of financial products and are subject to an evolving regulatory environment.
        </p>

        <p style={{ fontSize: 14 }}>
          © 2021 USDAO. All Rights Reserved
        </p>

      </div>


    </div >
  );
}

export default App;
